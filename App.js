import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  FlatList,
  AsyncStorage,
  ActivityIndicator,
  Platform,
} from "react-native";
import {
  createStackNavigator,
  createDrawerNavigator,
  createAppContainer,
  NavigationActions,
  NavigationAction
} from "react-navigation";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Text,
  Button,
  Left,
  Right,
  Body,
  Item,
  Card,
  CardItem,
  Input,
  Thumbnail,
  Fab,
  Icon
} from "native-base";

import SplashScreen from "./src/app/screen/SplashScreen";
import Menu from "./src/app/screen/Menu";
import Login from "./src/app/screen/Login";
import Register from "./src/app/screen/Register";
import MainMenu from "./src/app/screen/MainMenu";
import DetailStand from "./src/app/screen/DetailStand";
import Order from "./src/app/screen/Order";
import Bank from "./src/app/screen/Bank";
import TopUp from "./src/app/screen/TopUp";

const NavStack = createStackNavigator({
  SplashScreen: {
    screen: SplashScreen
  },
  Menu: {
    screen: Menu
  },
  Login: {
    screen: Login
  },
  Register: {
    screen: Register
  },
  MainMenu: {
    screen: MainMenu
  },
  DetailStand: {
    screen: DetailStand
  },
  Order: {
    screen: Order
  },
  Bank: {
    screen: Bank
  },
  TopUp: {
    screen: TopUp
  },

});

const App = createAppContainer(NavStack);

export default App;