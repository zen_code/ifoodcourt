import React, { Component } from 'react';
import {
  AppRegistry,
  Text,
  View,
  TouchableHighlight,
  NativeAppEventEmitter,
  Platform,
  PermissionsAndroid,
  AsyncStorage,
  Alert,
  ScrollView,
  FlatList,
  TouchableOpacity,
  RefreshControl,
  ActivityIndicator,
  Image
} from 'react-native';

import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Body,
    Icon,
    Thumbnail,
    Form,
    Item,
    Label,
    Input,
 } from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import styles from "./styles/AllStand";
import BleManager from 'react-native-ble-manager';
import GlobalConfig from "../components/GlobalConfig";
import SubBank from "../components/SubBank";
import colors from "../../styles/colors";
import ImagePicker from "react-native-image-picker";


export default class Bank extends Component {

    constructor(){
        super()
        this.state = {
            ble:null,
            scanning:false,
            token:'',
            listBank:[],
            topUp:false,
            idBank:'',
            namaBank:'',
            noRekening:'',
            namaNasabah:'',
            imageBank:'',
            pickedImage: '',
            uri: '',
            fileType:'',
        }
    }

    static navigationOptions = {
        header: null
    };

    pickImageHandler = () => {
      ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 800, maxHeight: 600 }, res => {
        if (res.didCancel) {
            console.log("User cancelled!");
        } else if (res.error) {
            console.log("Error", res.error);
        } else {
            this.setState({
                pickedImage: res.uri,
                uri: res.uri,
                fileType:res.type
            });
        }
      });
    }

    onRefresh() {
        console.log("refreshing");
        this.setState({ isLoading: true }, function() {
            this.loadBank();
        });
    }

    componentDidMount() {
        AsyncStorage.getItem("token").then(token => {
            this.setState({
              token: token,
              isLoading: false,
            });
            this.loadBank();
        });
    }

    loadBank(){
        this.setState({
            isLoading: true,
        })
        var token_Authorization = 'Bearer ' + JSON.parse(this.state.token);
 
        var url = GlobalConfig.SERVERHOST + 'bank';
        var formData = new FormData();
        formData.append()

        fetch(url, {
            headers: {
            'Content-Type': 'multipart/form-data',
            'Authorization': token_Authorization,
            },
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    listBank:responseJson,
                    isLoading: false,
                })
            })
            .catch((error) => {
            Alert.alert('Tidak Ada Internet', 'Cek Koneksi Internet Anda', [{
                text: 'Ok'
            }])
            console.log(error)
            })
    }

    topUp(idBank, namaBank, noRekening, namaNasabah, imageBank){
      this.setState({
        topUp:true,
        idBank:idBank,
        namaBank:namaBank,
        noRekening:noRekening,
        namaNasabah:namaNasabah,
        imageBank:imageBank,
      })
    }

    

    render() {

        // const container = {
        //     flex: 1,
        //     justifyContent: 'center',
        //     alignItems: 'center',
        //     backgroundColor: '#F5FCFF',
        // }

        that = this;
        var list;
        if (this.state.isLoading) {
          list = (
            <View
              style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
            >
              <ActivityIndicator size="large" color="#330066" animating />
            </View>
          );
        } else {
          if (this.state.listBank == '') {
            list = (
              <View
                style={{ flex: 1, justifyContent: "center", alignItems: "center", marginTop:200 }}
              >
                <Thumbnail
                  square
                  large
                  source={require("../../assets/images/empty.png")}
                />
                <Text>Tidak Ditemukan !</Text>
              </View>
            );
          } else {
            list = (
              <View style={{ flex: 0, paddingLeft:20, paddingRight:20 }}>
              {this.state.listBank.map((listBank, index) => (
                <TouchableOpacity
                transparent
                onPress={() => this.topUp(listBank.id, listBank.nama_bank, listBank.no_rekening, listBank.nama_nasabah, listBank.image)}>
                <View style={{width:'100%', marginRight:0, marginLeft:0, borderBottomWidth:1, borderBottomColor:'#DEDFDF'}}>
                  <View style={{flexDirection:'row', marginBottom:10, marginTop:5}}>
                    <View style={{width:'25%', justifyContent: "center", paddingLeft:5}}>
                    <View style={{width:70, height:70, borderRadius:40}}>
                        <Image
                          source={{
                            uri:
                              GlobalConfig.IMAGEHOST + listBank.image
                          }}
                          style={{ marginTop:0, width: 70, height: 70, resizeMode: "contain" }}
                        />
                      </View>
                    </View>
                    <View style={{width:'75%', paddingLeft:5}}>
                      <Text style={{fontSize:18, color:colors.black, fontWeight:'bold'}}>{listBank.nama_bank}</Text>
                      <Text style={{fontSize:15}}>{listBank.no_rekening}</Text>
                      <Text style={{fontSize:15}}>{listBank.nama_nasabah}</Text>
                    </View>
                  </View>
                </View>
                </TouchableOpacity>
              ))}
              </View>
            );
          }
        }

        return (
            <Container style={styles.wrapper}>
            <Header style={styles.header}>
              <Left style={{ flex: 1 }}>
                <Button transparent onPress={() => this.props.navigation.navigate("MainMenu")}>
                  <Icon
                    name="ios-arrow-back"
                    size={20}
                    style={styles.facebookButtonIconOrder2}
                  />
                </Button>
              </Left>
              <Body style={{ flex: 4, alignItems: "center" }}>
                <Title style={styles.textbody}>Pilih Metode Pembayaran</Title>
              </Body>
              <Right style={{ flex: 1 }}>
                
              </Right>
            </Header>
            <Content>
            <View style={{ marginTop: 5, marginRight:20}}>
              {list}
            </View>
            </Content>
            {/* <TouchableHighlight style={{padding:20, backgroundColor:'#ccc'}} onPress={() => this.toggleScanning(!this.state.scanning) }>
                     <Text>Scan Bluetooth ({this.state.scanning ? 'on' : 'off'})</Text>
                 </TouchableHighlight> */}

            <View style={{ width: 270, position: "absolute",}}>
            <Dialog
              visible={this.state.topUp}
              dialogAnimation={
                new SlideAnimation({
                  slideFrom: "bottom"
                })
              }
              dialogStyle={{ position: "absolute", top: this.state.posDialog, width:300 }}
              onTouchOutside={() => {
                this.setState({ topUp: false });
              }}
            >
              <DialogContent>
                    {
                    <View>
                      <View style={{alignItems:'center', justifyContent:'center', paddingTop:15, width:'100%', marginBottom:10}}>
                        <Text style={{alignItems:'center', color:colors.black, fontSize:15}}>UPLOAD BUKTI PEMBAYARAN</Text>
                      </View>
                      <View>
                      <View style={{flexDirection:'row', marginBottom:10, marginTop:5}}>
                        <View style={{width:'30%', justifyContent: "center", paddingLeft:5}}>
                        <View style={{width:70, height:70, borderRadius:40}}>
                            <Image
                              source={{
                                uri:
                                  GlobalConfig.IMAGEHOST + this.state.imageBank
                              }}
                              style={{ marginTop:0, width: 70, height: 70, resizeMode: "contain" }}
                            />
                          </View>
                        </View>
                        <View style={{width:'70%', paddingLeft:5}}>
                          <Text style={{fontSize:16, color:colors.black, fontWeight:'bold'}}>{this.state.namaBank}</Text>
                          <Text style={{fontSize:15}}>{this.state.noRekening}</Text>
                          <Text style={{fontSize:15}}>{this.state.namaNasabah}</Text>
                        </View>
                      </View>
                      </View>
                      
                      <View style={{flexDirection:'row', flex:1, paddingTop:0, width:'100%'}}>
                        <View style={{width:'100%', paddingRight:5}}>
                            <Form style={{ marginLeft: 0, marginRight:10, marginTop:5}}>
                                <Item stackedLabel>
                                <Label style={{fontSize:11}}>Nomimal</Label>
                                <View style={{flex:1, flexDirection:'row'}}>
                                    <Icon
                                        name='cash'
                                        style={{color:colors.black, fontSize:20, marginTop:12}}
                                    />
                                    <Input returnKeyType='next' keyboardType='numeric' placeholder='10000' value={this.state.nominal} onChangeText={(text) => this.setState({ nomimal: text })} />
                                </View>
                                </Item>
                                <Item stackedLabel>
                                <Label style={{fontSize:11}}>Bukti Pembayaran</Label>
                                <View style={{flex:1, flexDirection:'row'}}>
                                
                                View style={{marginLeft:25, marginRight:25, flex:1, flexDirection:'row', marginTop:-70}}>
                <View style={{width:'65%'}}>
                </View>
                <View style={{width:'35%', height:105, borderRadius:100, backgroundColor:'#F7F7F8'}}>
                  <Image source={{uri:this.state.pickedImage}} style={styles.previewImage}/>
                </View>
              </View>
              <View style={{marginLeft:25, marginRight:25, flex:1, flexDirection:'row', marginTop:-35}}>
                <View style={{width:'92%'}}>
                </View>
                <View style={{width:'8%', height:26, borderRadius:100, backgroundColor:'#5A5A5A', alignItems:'center', justifyContent:'center'}}>
                  <TouchableOpacity
                    transparent
                    style={{alignItems:'center', justifyContent:'center'}}
                    onPress={this.pickImageHandler}
                    >
                      <Icon
                        name='ios-add'
                        size={10}
                        style={{color:colors.white, fontSize:20, fontWeight:'bold'}}
                      />
                    </TouchableOpacity>
                </View>
                                </View>
                                </Item>
                            </Form>
     
                            <View style={{ flex: 1, flexDirection:'column', marginTop:20}}>
                                <View style={styles.Contentsave}>
                                    <Button
                                        block
                                        style={{
                                        width:'100%',
                                        height: 40,
                                        marginBottom: 10,
                                        borderWidth: 0,
                                        backgroundColor: colors.primary,
                                        borderRadius: 20
                                        }}
                                        onPress={() => this.konfirmasiLogin()}
                                    >
                                        <Text style={{color:colors.white}}>UPLOAD</Text>
                                    </Button>
                                </View>
                            </View>
                        </View>
                      </View>
                    </View>
                    }
                  </DialogContent>
            </Dialog>
            </View>
            </Container>




            // <View style={container}>

            //     {/* <Button onPress={() => this.toggleScanning(!this.state.scanning)} title="Start scanning"/> */}

            //     <TouchableHighlight style={{padding:20, backgroundColor:'#ccc'}} onPress={() => this.toggleScanning(!this.state.scanning) }>
            //         <Text>Scan Bluetooth ({this.state.scanning ? 'on' : 'off'})</Text>
            //     </TouchableHighlight>

            //     {bleList}
            // </View>
        );
    }
}
