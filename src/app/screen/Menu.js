import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Card,
  CardItem,
  Form,
  Item,
  Label,
  Input,
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import styles from "./styles/Login";
import colors from "../../styles/colors";
import GlobalConfig from "../components/GlobalConfig";

export default class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading : false,
      login:false,
      register:false,
      email:'fahmisyaifudin00@gmail.com',
      password:'123456',
    };
  }

  static navigationOptions = {
    header: null
  };

  login(){
    this.setState({
      login: true
    });
  }

  register(){
    this.props.navigation.navigate('Register');
  }

  logOut(){
    Alert.alert(
      'Konfirmasi',
      'Apakah Anda ingin keluar dari aplikasi?',
      [
        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'Yes', onPress: () => BackHandler.exitApp() },
      ],
      { cancelable: false }
    );
  }

  konfirmasiLogin(){
    if(this.state.email==""){
      Alert.alert(
        'Peringatan',
        'Masukkan Email',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    }
    else if(this.state.password==""){
      Alert.alert(
        'Peringatan',
        'Masukkan Password',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    } else {
      var url = GlobalConfig.SERVERHOST + 'login';
      var formData = new FormData();
      formData.append("email", this.state.email)
      formData.append("password", this.state.password)

      fetch(url, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        method: 'POST',
        body: formData
      }).then((response) => response.json())
        .then((response) => {
            if(response.error != 'Unauthorised') {
                this.setState({
                    login:false
                })
                AsyncStorage.setItem('token', JSON.stringify(response.success.token)).then(() => {
                    this.props.navigation.navigate("MainMenu");
                })
            } else {
                Alert.alert(
                  'Login',
                  'Password Salah',
                  [
                    { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                  ],
                  { cancelable: false }
                );
            }
        })
        .catch((error) => {
          // Alert.alert('Cannot Log in', 'Check Your Internet Connection', [{
          //   text: 'Ok'
          // }])
          this.props.navigation.navigate("MainMenu");
          this.setState({
                login:false
            })
        //   console.log(error)
        })
    }
  }


  render() {
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
      <View style={styles.homeWrapper}>
        <View
          style={{ flex: 1, flexDirection: "column", backgroundColor: "#fff" }}
        >
          <View>
            <ScrollView>
              <ImageBackground
                style={{
                  alignSelf: "center",
                  width: Dimensions.get("window").width,
                  height: Dimensions.get("window").height,
                }}
                source={require("../../assets/images/background.png")}>
                <TouchableOpacity
                  transparent
                  style={{position:'absolute',top:((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?40:20,right:320}}
                  onPress={()=>this.logOut()}
                >
                </TouchableOpacity>
              </ImageBackground>
              <View style={{ flex: 1, flexDirection:'column', marginTop:-140, marginLeft:20, marginRight:20}}>
                <View>
                  <Button
                    block
                    style={{
                      width:'100%',
                      height: 40,
                      marginBottom: 10,
                      borderWidth: 0,
                      backgroundColor: colors.secondary,
                      borderRadius: 15
                    }}
                    onPress={() => this.login()}
                  >
                    <Text style={{color:colors.white}}>MASUK</Text>
                  </Button>
                </View>
                <View>
                  <Button
                    block
                    style={{
                      width:'100%',
                      height: 40,
                      marginBottom: 25,
                      borderWidth: 0,
                      backgroundColor: colors.primary,
                      borderRadius: 15
                    }}
                    onPress={() => this.register()}
                  >
                    <Text style={{color:colors.white}}>DAFTAR</Text>
                  </Button>
                </View>
              </View>
            </ScrollView>
            <View style={{ width: '100%', position: "absolute"}}>
                <Dialog
                  visible={this.state.login}
                  dialogAnimation={
                    new SlideAnimation({
                      slideFrom: "bottom"
                    })
                  }
                  dialogStyle={{ position: "absolute", top: this.state.posDialog, width:300}}
                  onTouchOutside={() => {
                    this.setState({ login: false });
                  }}
                >
                  <DialogContent>
                    {
                    <View>
                      <View style={{alignItems:'center', justifyContent:'center', paddingTop:15, width:'100%'}}>
                        <Text style={{alignItems:'center', color:colors.black, fontSize:20}}>LOGIN</Text>
                      </View>
                      <View style={{flexDirection:'row', flex:1, paddingTop:0, width:'100%'}}>
                        <View style={{width:'100%', paddingRight:5}}>
                            <Form style={{ marginLeft: 0, marginRight:10, marginTop:5}}>
                                <Item stackedLabel>
                                <Label style={styles.weatherText}>Email</Label>
                                <View style={{flex:1, flexDirection:'row'}}>
                                    <Icon
                                        name='contact'
                                        style={{color:colors.black, fontSize:20, marginTop:12}}
                                    />
                                    <Input returnKeyType='next' value={this.state.email} onChangeText={(text) => this.setState({ email: text })} />
                                </View>
                                </Item>
                                <Item stackedLabel>
                                <Label style={styles.weatherText}>Password</Label>
                                <View style={{flex:1, flexDirection:'row'}}>
                                    <Icon
                                        name='unlock'
                                        style={{color:colors.black, fontSize:20, marginTop:12}}
                                    />
                                    <Input returnKeyType='go' secureTextEntry={true} value={this.state.password} onChangeText={(text) => this.setState({ password: text })} />
                                </View>
                                </Item>
                            </Form>
     
                            <View style={{ flex: 1, flexDirection:'column', marginTop:20}}>
                                <View style={styles.Contentsave}>
                                    <Button
                                        block
                                        style={{
                                        width:'100%',
                                        height: 40,
                                        marginBottom: 10,
                                        borderWidth: 0,
                                        backgroundColor: colors.primary,
                                        borderRadius: 20
                                        }}
                                        onPress={() => this.konfirmasiLogin()}
                                    >
                                        <Text style={{color:colors.white}}>LOGIN</Text>
                                    </Button>
                                </View>
                            </View>
                        </View>
                      </View>
                    </View>
                    }
                  </DialogContent>
                </Dialog>
              </View>             
          </View>
        </View>
        </View>
        </Container>
      );
  }
}
