import React, { Component } from 'react';
import {
  AppRegistry,
  Text,
  View,
  TouchableHighlight,
  NativeAppEventEmitter,
  Platform,
  PermissionsAndroid,
  AsyncStorage,
  Alert,
  ScrollView,
  FlatList,
  TouchableOpacity,
  RefreshControl,
  ActivityIndicator,
  Image
} from 'react-native';

import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Body,
    Icon,
    Thumbnail,
    Input,
 } from "native-base";
 import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import styles from "./styles/AllStand";
import BleManager from 'react-native-ble-manager';
import GlobalConfig from "../components/GlobalConfig";
import SubMenu from "../components/SubMenu";
import colors from "../../styles/colors";

var that;
class ListItem extends React.PureComponent {

  constructor(props){
    super(props)
    this.state = {
      listMenu:[],

    }
}

  navigateToScreen(route, menu) {
    // AsyncStorage.setItem("kodeStand", JSON.stringify(kodeStand)).then(() => {
    //   that.props.navigation.navigate(route);
    // });
    alert(JSON.stringify(this.state.listMenu))
  }

  

  render() {
    return (
      <View style={{width:'100%', marginRight:0, borderBottomWidth:1, borderBottomColor:'#DEDFDF', marginLeft:15}}>
      <TouchableOpacity
        transparent
        onPress={() => this.push(this.props.data.menu)}>
        <SubMenu
            imageUri={{ uri: GlobalConfig.IMAGEHOST + this.props.data.image }}
            menu={this.props.data.menu}
            harga={this.props.data.harga}
          />
        </TouchableOpacity>
        <TouchableOpacity
        transparent
        onPress={() => this.navigateToScreen("DetailStand", this.props.data.menu)}>
        <SubMenu
            imageUri={{ uri: GlobalConfig.IMAGEHOST + this.props.data.image }}
            menu={this.props.data.menu}
            harga={this.props.data.harga}
          />
        </TouchableOpacity>
      </View>
    );
  }
  }

export default class Order extends Component {

    constructor(){
        super()
        this.state = {
            ble:null,
            scanning:false,
            token:'',
            kodeStand:'',
            stand:'',
            image:'',

            listNama:[],
            listHarga:[],
            listJumlah:[],
            listTotal:[],
            total:'',
            visibleKonfirmasi:false,
            visibleDialogSubmit:false,
            visibleQR:false,
            listOrder:[],
            qrcode:'',
            
            
            idMenu:[],
            popUp:false,
            namaMenu:[],
            hargaMenu:[],
            jumlah:[]
        }
    }

    static navigationOptions = {
        header: null
    };

    onRefresh() {
        console.log("refreshing");
        this.setState({ isLoading: true }, function() {
            this.loadMenu();
        });
      }

    componentDidMount() {
        AsyncStorage.getItem("token").then(token => {
          AsyncStorage.getItem("arrNama").then(arrNama => {
            AsyncStorage.getItem("arrHarga").then(arrHarga => {
              var arrayNama = JSON.parse(arrNama);
              var arrayHarga = JSON.parse(arrHarga);

              this.setState({
                token: token,
                // listNama: arrNama,
                // listHarga: arrHarga,
                isLoading: false,
              });
           
              var arrN = this.state.listNama;
              var arrH = this.state.listHarga;
              var arrJ = this.state.listJumlah;
              var tempDataN = arrayNama;
              var tempDataH = arrayHarga;
    
              var total = 0;
              for (let index = 0; index < arrayNama.length; index++) {
                arrN[index] = tempDataN[index];
                arrH[index] = tempDataH[index];
                arrJ[index] = 1;
                total = parseInt(total) + parseInt(arrH[index]);
              }
              
              this.setState({
                listNama: arrN,
                listHarga: arrH,
                listJumlah: arrJ,
                total: total,
              });
            })
          })
        });

    }

    sendOrder(){  
      for (let i = 0; i < this.state.listNama.length; i++) {
        let nama = this.state.listNama[i];
        let harga = this.state.listHarga[i];
        let jumlah = this.state.listJumlah[i];
  
        var temp = {'menu': nama, 'jumlah': jumlah, 'harga':harga};
            var data = this.state.listOrder;

            data.push(temp);
  
            this.setState({
              listOrder:data,
            })
      }
      this.sendOrderFinal()     
    }

    sendOrderFinal(){
        this.setState({ visibleKonfirmasi: false })
        this.setState({
          textDialogSubmit:'Membuat pesanan...',
        })
        this.setState({
          visibleDialogSubmit:true,
        })

        var token_Authorization = 'Bearer ' + JSON.parse(this.state.token);
 
        var url = GlobalConfig.SERVERHOST + 'order';
        var formData = new FormData();
        formData.append("detail",  JSON.stringify(this.state.listOrder))

        fetch(url, {
            headers: {
            'Content-Type': 'multipart/form-data',
            'Authorization': token_Authorization,
            },
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                  visibleDialogSubmit:false,
                  qrcode : responseJson.image,
                  visibleQR:true,
                })
            })
            .catch((error) => {
            Alert.alert('Tidak Bisa Pesan', 'Cek Koneksi Internet Anda', [{
                text: 'Ok'
            }])
            console.log(error)
            })
    }

    backMenu(){
      this.setState({
        visibleQR:false
      })
      this.props.navigation.navigate('MainMenu')
    }

    convertRp(angka){
      var rupiah = '';		
      var angkarev = angka.toString().split('').reverse().join('');
      for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
      return ( 'Rp. '+rupiah.split('',rupiah.length-1).reverse().join(''))
    }

    valueJumlah(index) {
      let arr = this.state.listJumlah;
      if (arr[index] == undefined) {
        return "1";
        console.log("masuk sini");
      } else {
        return this.state.listJumlah[index];
      }
      this.total()
    }
  
    ubahJumlah(jumlah, index) {
      let arr = this.state.listJumlah;
      arr[index] = jumlah;
      this.setState({
        listJumlah: arr
      });
      this.total()
    }
  
    tambahJumlah(index) {
      let arr = this.state.listJumlah;
  
      arr[index] = arr[index] + 1;
      this.setState({
        listJumlah: arr
      });

      this.total()
  
      // console.log(this.state.listJumlah[index]);
    }
  
    kurangJumlah(index) {
      let arr = this.state.listJumlah;
      if (arr[index] == 1 || arr[index] == null || arr[index] == undefined) {
      } else {
        arr[index] = arr[index] - 1;
        this.setState({
          listJumlah: arr
        });
      }
      this.total()
    }

    total(){
      var arrJumlah = this.state.listJumlah;
      var arrHarga = this.state.listHarga;
      var arrTotal = this.state.listTotal;

      // var tempJumlah = arrJumlah;
      // var tempHarga = arrHarga;
      for (let index2 = 0; index2 < this.state.listNama.length; index2++) {
          arrTotal[index2] = arrJumlah[index2] * arrHarga[index2];
      }
      this.setState({
        listTotal: arrTotal,
      });

      var arrBayar = this.state.listTotal;
      var total = 0;

      for (let index3 = 0; index3 < this.state.listTotal.length; index3++) {
        total = total + arrBayar[index3];
      }

      this.setState({
        total: total,
      });
    }

    render() {
        that = this;
        var list;
        if (this.state.isLoading) {
          list = (
            <View
              style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
            >
              <ActivityIndicator size="large" color="#330066" animating />
            </View>
          );
        } else {
          if (this.state.listMenu == '') {
            list = (
              <View
                style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
              >
                <Thumbnail
                  square
                  large
                  source={require("../../assets/images/empty.png")}
                />
                <Text>No Data!</Text>
              </View>
            );
          } else {
            list = (
              <View style={{ flex: 0, paddingLeft:20, paddingRight:20 }}>
              {this.state.listNama.map((listNama, index) => (
                <View style={{width:'100%', marginRight:0, marginLeft:0, borderBottomWidth:1, borderBottomColor:'#DEDFDF'}}>
                  <View style={{flexDirection:'row', marginBottom:15, marginTop:0}}>
                    <View style={{width:'70%', justifyContent: "center", paddingLeft:5, marginTop:15}}>
                      <Text style={{fontSize:16, color:colors.black, fontWeight:'bold'}}>{listNama}</Text>
                      <Text style={{fontSize:16, color:colors.primary}}>{this.convertRp(this.state.listHarga[index])} x {this.state.listJumlah[index]} 
                      = {this.convertRp(this.state.listHarga[index] * this.state.listJumlah[index])}</Text>
                    </View>
                    <View style={{width:'40%', marginTop:10}}>
                        <View style={{ flex: 1, flexDirection: "row"}}>
                          <Button onPress={() => this.kurangJumlah(index)} style={styles.btnQTYLeft}>
                              <Icon
                                name="ios-arrow-back"
                                style={styles.facebookButtonIconQTY}
                              />
                          </Button>

                          <View style={{width:30, height:30, marginTop: 15, backgroundColor: colors.white}}>
                            <Input
                              style={{
                                height:30,
                                marginTop:-5,
                                fontSize:12,
                                textAlign:'center'}}
                              value={this.state.listJumlah[index] + ""}
                              keyboardType='numeric'
                              onChangeText={text => this.ubahJumlah(text, index)}
                            />
                          </View>

                          <Button onPress={() => this.tambahJumlah(index)} style={styles.btnQTYRight}>
                            <Icon
                              name="ios-arrow-forward"
                              style={styles.facebookButtonIconQTY}
                            />
                          </Button>

                          {/* <View style={styles.btnWishlist}>
                            <Button transparent onPress={() => this.deleteWishlist(apdListUnitKerja.id, apdListUnitKerja.kode_apd)}>
                              <Icon
                                name="ios-trash"
                                style={styles.facebookButtonIconDangerous}
                              />
                            </Button>
                          </View> */}
                        </View>
                    </View>
                  </View>
                </View>
              ))}
              </View>
            );
          }
        }

        return (
            <Container style={styles.wrapper}>
            <Header style={styles.header}>
              <Left style={{ flex: 1 }}>
                <Button
                  transparent
                  onPress={() => this.props.navigation.navigate("MainMenu")}
                >
                  <Icon
                    name="ios-arrow-back"
                    size={20}
                    style={styles.facebookButtonIconOrder2}
                  />
                </Button>
              </Left>
              <Body style={{ flex: 3, alignItems: "center" }}>
                <Title style={styles.textbody}>Pesan</Title>
              </Body>
              <Right style={{ flex: 1 }}>
                {/* <Button transparent onPress={() => this.submit()}>
                  <Icon
                    name="ios-cart"
                    size={20}
                    style={styles.facebookButtonIconOrder2}
                  />
                </Button> */}
              </Right>
            </Header>
            <Content>
            <View style={{ marginTop: 5, marginRight:0}}>
              {list}
            </View>
            </Content>
            <Footer>
              <FooterTab style={{ backgroundColor:'#DEDFDF'}}>
                <View style={{flex:1, flexDirection:'row', paddingRight:10}}>
                  <View style={{width:'45%', justifyContent:'center'}}>
                    <View style={{paddingTop:15, paddingLeft:15, paddingBottom:15}}>
                      <Text style={{fontSize:20}}>{this.convertRp(this.state.total)}</Text>

                    </View>
                  </View>
                  <View style={{width:'55%', justifyContent: 'center'}}>
                      <Button
                        block
                        style={{
                          width:'100%',
                          height: 40,
                          margin: 5,
                          borderWidth: 0,
                          backgroundColor: colors.primary,
                          borderRadius: 25
                        }}
                        onPress={() => this.setState({
                          visibleKonfirmasi:true
                        })}
                      >
                        <Text style={{color:colors.white, fontWeight:'bold', fontSize:15}}>BAYAR</Text>
                    </Button>
                  </View>
                </View>
              </FooterTab>
            </Footer>

            <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleDialogSubmit}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogTitle={<DialogTitle title={this.state.textDialogSubmit} />}
          >
            <DialogContent>
              {

                  <ActivityIndicator size="large" color="#330066" animating />

              }
            </DialogContent>
          </Dialog>
        </View>
        <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleKonfirmasi}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            onTouchOutside={() => {
              this.setState({ visibleKonfirmasi: false });
            }}
            // dialogStyle={{ position: 'absolute', top: this.state.posDialog }}
            dialogTitle={
              <DialogTitle
                title="Apakah yang Anda pesan sudah sesuai?"
                style={{
                  backgroundColor: colors.white,
                  marginTop:5,
                  marginBottom:5
                }}
                textStyle={{fontSize:15}}
                hasTitleBar={false}
                align="center"
              />
            }
          >
            <DialogContent
              style={{
                backgroundColor: colors.white
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 1 }}>
                  <Button
                    block
                    style={{
                      height: 45,
                      marginLeft: 20,
                      marginRight: 20,
                      marginBottom: 20,
                      borderWidth: 1,
                      backgroundColor: colors.primary,
                      borderColor: colors.primary,
                      borderRadius: 4
                    }}
                    onPress={() => this.setState({ visibleKonfirmasi: false })}
                  >
                    <Text style={{color:colors.white}}>Batal</Text>
                  </Button>
                </View>
                <View style={{ flex: 1 }}>
                  <Button
                    block
                    style={{
                      height: 45,
                      marginLeft: 20,
                      marginRight: 20,
                      marginBottom: 20,
                      borderWidth: 1,
                      backgroundColor: colors.primary,
                      borderColor: colors.primary,
                      borderRadius: 4,
                    }}
                    onPress={() => this.sendOrder()}
                  >
                    <Text style={{color:colors.white}}>Iya</Text>
                  </Button>
                </View>
              </View>
            </DialogContent>
          </Dialog>
        </View>

        <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleQR}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            onTouchOutside={() => {
              this.backMenu()
            }}
            // dialogStyle={{ position: 'absolute', top: this.state.posDialog }}
            dialogTitle={
              <DialogTitle
                title="Pesanan Berhasil"
                style={{
                  backgroundColor: colors.white,
                  marginTop:-10,
                  marginBottom:-10
                }}
                textStyle={{fontSize:20}}
                hasTitleBar={false}
                align="center"
              />
            }
          >
            <DialogContent
              style={{
                backgroundColor: colors.white
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <View style={{width:190, height:190, borderRadius:0}}>
                  <Image
                    source={{
                      uri: this.state.qrcode
                    }}
                    style={{ width: '100%', height: '100%', marginTop: 0, marginBottom: 2, borderRadius:10,}}
                  />
                </View>
              </View>
              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 1 }}>
                  <Button
                    block
                    style={{
                      height: 45,
                      marginLeft: 20,
                      marginRight: 20,
                      marginBottom: 5,
                      marginTop:20,
                      borderWidth: 1,
                      backgroundColor: colors.primary,
                      borderColor: colors.primary,
                      borderRadius: 4,
                    }}
                    onPress={() => this.backMenu()}
                  >
                    <Text style={{color:colors.white}}>Keluar</Text>
                  </Button>
                </View>
              </View>
            </DialogContent>
          </Dialog>
        </View>

        </Container>
        );
    }
}
