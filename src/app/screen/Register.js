import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Card,
  CardItem,
  Form,
  Item,
  Label,
  Input,
  Textarea
} from "native-base";
import styles from "./styles/Login";
import colors from "../../styles/colors";
// import CheckBox from 'react-native-check-box';
// import CustomRadioButton from "../components/CustomRadioButton";
// import ImagePicker from "react-native-image-picker";
import GlobalConfig from "../components/GlobalConfig";

export default class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading : false,
      isAgre:false,
      name:'',
      email:'',
      password:'',
      cPassword: '',
    };
  }

  static navigationOptions = {
    header: null
  };


  konfirmasiRegister(){
    if(this.state.name==""){
      Alert.alert(
        'Informasi',
        'Masukkan Nama Anda',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    }
    else if(this.state.email==""){
      Alert.alert(
        'Informasi',
        'Masukkan Email Anda',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    }
    else if(this.state.password==""){
      Alert.alert(
        'Informasi',
        'Masukkan Password Anda',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    }
    else if(this.state.cPassword==""){
      Alert.alert(
        'Informasi',
        'Masukkan Ulang Password Anda',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    }
    else if(this.state.password!=this.state.cPassword){
      Alert.alert(
        'Informasi',
        'Password Anda Berbeda',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    }
    else {
          var url = GlobalConfig.SERVERHOST + 'register';
          var formData = new FormData();
          formData.append("name", this.state.name)
          formData.append("email", this.state.email)
          formData.append("password", this.state.password)
          formData.append("c_password", this.state.cPassword)
          
          fetch(url, {
            headers: {
              'Content-Type': 'multipart/form-data'
            },
            method: 'POST',
            body: formData
          }).then((response) => response.json())
            .then((response) => {
              if(response.error != 'Unauthorised') {
                AsyncStorage.setItem('token', JSON.stringify(response.success.token)).then(() => {
                  Alert.alert('Success', 'Registrasi Berhasil', [{
                      text: 'OK'
                  }])
                  this.props.navigation.navigate("MainMenu");
                })

              } else{
                Alert.alert('Error', 'Register Failed', [{
                  text: 'OK'
                }]);
              }
          });
      }
  }


  render() {
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
      <View style={styles.homeWrapper}>
        <View
          style={{ flex: 1, flexDirection: "column", backgroundColor: "#fff", width:'100%' }}
        >
          <View>
            <ScrollView>
              <ImageBackground
                style={{
                  alignSelf: "center",
                  width: Dimensions.get("window").width,
                  height: 135,
                }}
                source={require("../../assets/images/register.png")}>
                <TouchableOpacity
                  transparent
                  style={{position:'absolute',top:((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?40:20,right:320}}
                  onPress={()=>this.props.navigation.navigate("Menu")}
                >
                  <Icon
                    name='arrow-back'
                    size={10}
                    style={{color:colors.white, fontSize:20}}
                  />
                </TouchableOpacity>
              </ImageBackground>
              
              <Card style={{ marginLeft: 25, marginRight: 25, borderRadius: 50, marginTop:100, paddingLeft:10, paddingRight:10}}>
                <Textarea style={{fontSize:14, color:colors.gray}} rowSpan={1.8} value={this.state.name} placeholder='Nama' onChangeText={(text) => this.setState({ name: text })} />
              </Card>
              <Card style={{ marginLeft: 25, marginRight: 25, borderRadius: 50, marginTop:20, paddingLeft:10, paddingRight:10}}>
                <Textarea style={{fontSize:14, color:colors.gray}} rowSpan={1.8} value={this.state.email} placeholder='Email' onChangeText={(text) => this.setState({ email: text })} />
              </Card>
              <Card style={{ marginLeft: 25, marginRight: 25, borderRadius: 50, marginTop:20, paddingLeft:10, paddingRight:10}}>
                <Input style={{fontSize:14, color:colors.gray}} secureTextEntry={true} value={this.state.password} placeholder='Password' onChangeText={(text) => this.setState({ password: text })} />    
              </Card>
              <Card style={{ marginLeft: 25, marginRight: 25, borderRadius: 50, marginTop:20, paddingLeft:10, paddingRight:10}}>
                <Input style={{fontSize:14, color:colors.gray}} secureTextEntry={true} value={this.state.cPassword} placeholder='Konfirmasi Password' onChangeText={(text) => this.setState({ cPassword: text })} />
              </Card>
              
              <View style={{paddingLeft:25, paddingRight:25, marginTop:15}}>
                {/* <CheckBox
                  style={{flex:1, paddingTop:0}}
                  onClick={()=>{
                    this.setState({
                      isAgre:!this.state.isAgre,
                    })
                  }}
                  isChecked={this.state.isAgre}
                  rightText={"I accept all terms and conditions"}
                /> */}
              </View>
              <View style={{marginLeft:100, marginRight:100, marginTop:30}}>
                <Button
                  block
                  style={{
                    width:'100%',
                    height: 40,
                    marginBottom: 20,
                    borderWidth: 0,
                    backgroundColor: colors.primary,
                    borderRadius: 20
                  }}
                  onPress={() => this.konfirmasiRegister()}
                >
                  <Text style={{color:colors.white}}>DAFTAR</Text>
                </Button>
              </View>
            </ScrollView>
          </View>
        </View>
        </View>
        </Container>
      );
  }
}
