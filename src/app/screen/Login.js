import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  BackHandler
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Card,
  Icon,
  CardItem,
  Form,
  Item,
  Label,
  Input,
} from "native-base";
import styles from "./styles/Login";
import colors from "../../styles/colors";
import GlobalConfig from "../components/GlobalConfig";

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading : false,
      // email:'',
      // password:'',
      email:'fahmisyaifudin00@gmail.com',
      password:'123456',
      visibleLoading: false,
    };
  }

  static navigationOptions = {
    header: null
  };

  logOut(){
    Alert.alert(
      'Konfirmasi',
      'Apakah Anda ingin keluar dari aplikasi?',
      [
        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'Yes', onPress: () => BackHandler.exitApp() },
      ],
      { cancelable: false }
    );
  }

  konfirmasiLogin(){
    if(this.state.email==""){
      Alert.alert(
        'Peringatan',
        'Masukkan Email',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    }
    else if(this.state.password==""){
      Alert.alert(
        'Peringatan',
        'Masukkan Password',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    } else {
      var url = GlobalConfig.SERVERHOST + 'login';
      var formData = new FormData();
      formData.append("email", this.state.email)
      formData.append("password", this.state.password)

      fetch(url, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        method: 'POST',
        body: formData
      }).then((response) => response.json())
        .then((response) => {
            if(response.error != 'Unauthorised') {
                AsyncStorage.setItem('token', JSON.stringify(response.success.token)).then(() => {
                    this.props.navigation.navigate("MainMenu");
                })
            } else {
                Alert.alert(
                  'Login',
                  'Password Salah',
                  [
                    { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                  ],
                  { cancelable: false }
                );
            }
        })
        .catch((error) => {
          Alert.alert('Cannot Log in', 'Check Your Internet Connection', [{
            text: 'Ok'
          }])
          // this.props.navigation.navigate("MainMenu");
          console.log(error)
        })
    }
  }


  render() {
    return (
      <Container>
     <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
      <View style={styles.homeWrapper}>
        <View
          style={{ flex: 1, flexDirection: "column", backgroundColor: "#fff" }}
        >
          <View>
            <ScrollView>
              <ImageBackground
                style={{
                  alignSelf: "center",
                  width: Dimensions.get("window").width,
                  height: 300,
                  marginTop:-2,
                }}
                source={require("../../assets/images/head-img.png")}>
                <TouchableOpacity
                  transparent
                  style={{position:'absolute',top:((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?40:20,right:320}}
                  onPress={()=>this.logOut()}
                >
                  <Icon
                    name='arrow-back'
                    size={10}
                    style={{color:colors.white, fontSize:20}}
                  />
                </TouchableOpacity>
              </ImageBackground>
              <Card style={{ marginLeft: 20, marginRight: 20, borderRadius: 20, marginTop:-100, paddingBottom:40 }}>
              <View>
                <Text style={styles.signIn}>Login</Text>
              </View>
              <Form style={{ marginLeft: 0, marginRight:10, marginTop:5}}>
                <Item stackedLabel>
                  <Label style={styles.weatherText}>Email</Label>
                  <View style={{flex:1, flexDirection:'row'}}>
                      <Icon
                        name='contact'
                        style={{color:colors.black, fontSize:25, marginTop:12}}
                      />
                      <Input returnKeyType='next' value={this.state.email} onChangeText={(text) => this.setState({ email: text })} />
                  </View>
                </Item>
                <Item stackedLabel>
                  <Label style={styles.weatherText}>Password</Label>
                  <View style={{flex:1, flexDirection:'row'}}>
                      <Icon
                        name='unlock'
                        size={10}
                        style={{color:colors.black, fontSize:25, marginTop:12}}
                      />
                      <Input returnKeyType='go' secureTextEntry={true} value={this.state.password} onChangeText={(text) => this.setState({ password: text })} />
                  </View>
                </Item>
              </Form>
              </Card>
              <CardItem style={{ borderRadius: 0, marginTop:140}}>
                <View style={{ flex: 1, flexDirection:'column'}}>
                <View style={styles.Contentsave}>
                  <Button
                    block
                    style={{
                      width:'100%',
                      height: 45,
                      marginBottom: 20,
                      borderWidth: 0,
                      backgroundColor: colors.primary,
                      borderRadius: 15
                    }}
                    onPress={() => this.konfirmasiLogin()}
                  >
                    <Text style={{color:colors.white}}>LOGIN</Text>
                  </Button>
                </View>
                </View>
              </CardItem>
            </ScrollView>
          </View>
        </View>
        </View>
      </Container>
    );
  }
}
