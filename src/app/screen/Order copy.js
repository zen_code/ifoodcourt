import React, { Component } from 'react';
import {
  AppRegistry,
  Text,
  View,
  TouchableHighlight,
  NativeAppEventEmitter,
  Platform,
  PermissionsAndroid,
  AsyncStorage,
  Alert,
  ScrollView,
  FlatList,
  TouchableOpacity,
  RefreshControl,
  ActivityIndicator,
  Image
} from 'react-native';

import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Body,
    Icon,
    Thumbnail
 } from "native-base";
 import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import styles from "./styles/AllStand";
import BleManager from 'react-native-ble-manager';
import GlobalConfig from "../components/GlobalConfig";
import SubMenu from "../components/SubMenu";
import colors from "../../styles/colors";

var that;
class ListItem extends React.PureComponent {

  constructor(props){
    super(props)
    this.state = {
      listMenu:[],

    }
}

  navigateToScreen(route, menu) {
    // AsyncStorage.setItem("kodeStand", JSON.stringify(kodeStand)).then(() => {
    //   that.props.navigation.navigate(route);
    // });
    alert(JSON.stringify(this.state.listMenu))
  }

  

  render() {
    return (
      <View style={{width:'100%', marginRight:0, borderBottomWidth:1, borderBottomColor:'#DEDFDF', marginLeft:15}}>
      <TouchableOpacity
        transparent
        onPress={() => this.push(this.props.data.menu)}>
        <SubMenu
            imageUri={{ uri: GlobalConfig.IMAGEHOST + this.props.data.image }}
            menu={this.props.data.menu}
            harga={this.props.data.harga}
          />
        </TouchableOpacity>
        <TouchableOpacity
        transparent
        onPress={() => this.navigateToScreen("DetailStand", this.props.data.menu)}>
        <SubMenu
            imageUri={{ uri: GlobalConfig.IMAGEHOST + this.props.data.image }}
            menu={this.props.data.menu}
            harga={this.props.data.harga}
          />
        </TouchableOpacity>
      </View>
    );
  }
  }

export default class Order extends Component {

    constructor(){
        super()
        this.state = {
            ble:null,
            scanning:false,
            token:'',
            kodeStand:'',
            stand:'',
            image:'',
            listOrder:[],
            idMenu:[],
            popUp:false,
            namaMenu:[],
            hargaMenu:[],
            jumlah:[]
        }
    }

    static navigationOptions = {
        header: null
    };

    onRefresh() {
        console.log("refreshing");
        this.setState({ isLoading: true }, function() {
            this.loadMenu();
        });
      }

    componentDidMount() {
        AsyncStorage.getItem("token").then(token => {
          AsyncStorage.getItem("listOrder").then(listOrder => {
            alert(listOrder)
            // this.setState({
            //   token: token,
            //   listOrder: listOrder,
            //   isLoading: false,
            // });
            // var arr = new Array(this.state.listOrder.length);
           
            // var tempData = this.state.dataSource;
            // for (let index = 0; index < arr.length; index++) {
            //   arr[index] = parseInt(tempData[index].jumlah)
            // }
            // this.setState({
            //   jumlahApd: arr
            // });
          })
        });

    }

    push(nama, harga, id){
      var order = JSON.stringify(this.state.idMenu);
      var validasi = order.search(id);

      if(validasi =='-1'){
        var temp = {'nama': nama, 'harga': harga};
        var temp2 = {'id' : id};
        var data = this.state.listOrder;
        var data2 = this.state.idMenu;

        data.push(temp);
        data2.push(temp2);
      
        this.setState({
          listOrder:data,
          idMenu:data2,
        })
        this.setState({popUp:true})
      } else {
        this.setState({popUp:true})
        alert("Menu Sudah Ada Dalam Keranjang")
      }      
    }

    close(){
      this.setState({
        popUp:false,
      })
    }
    navigateToScreen(route, listOrder) {
      // AsyncStorage.setItem("listOrder", JSON.stringify(listOrder)).then(() => {
      //   that.props.navigation.navigate(route);
      // });
      alert(JSON.stringify(listOrder))
    }

    render() {
        that = this;
        var list;
        if (this.state.isLoading) {
          list = (
            <View
              style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
            >
              <ActivityIndicator size="large" color="#330066" animating />
            </View>
          );
        } else {
          if (this.state.listMenu == '') {
            list = (
              <View
                style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
              >
                <Thumbnail
                  square
                  large
                  source={require("../../assets/images/empty.png")}
                />
                <Text>No Data!</Text>
              </View>
            );
          } else {
            list = (
              <View style={{ flex: 0, paddingLeft:20, paddingRight:20 }}>
              {/* {this.state.listOrder.map((listMenu, index) => (
                <TouchableOpacity
                transparent
                onPress={() => this.push(listMenu.menu, listMenu.harga, listMenu.id_menu)}>
                <View style={{width:'100%', marginRight:0, marginLeft:0, borderBottomWidth:1, borderBottomColor:'#DEDFDF'}}>
                  <View style={{flexDirection:'row', marginBottom:10, marginTop:5}}>
                    <View style={{width:'22%', justifyContent: "center", paddingLeft:5}}>
                    <View style={{width:60, height:60, borderRadius:40}}>
                        <Image
                          source={{
                            uri:
                              GlobalConfig.IMAGEHOST + listMenu.image
                          }}
                          style={{ width: '100%', height: '100%', marginTop: 0, marginBottom: 2, borderRadius:10,}}
                        />
                      </View>
                    </View>
                    <View style={{width:'68%', paddingLeft:5}}>
                    <Text style={{fontSize:16, color:colors.black, fontWeight:'bold'}}>{listMenu.menu}</Text>
                    <Text style={{fontSize:15, color:colors.primary}}>Rp. {listMenu.harga}</Text>
                    </View>
                    <View style={{width:'10%', justifyContent: "center", paddingLeft:5}}>
                      <Icon
                        name="ios-cart"
                        size={20}
                        style={{fontSize:25,fontWeight:'bold', color:colors.primary}}
                      />
                    </View>
                  </View>
                </View>
                </TouchableOpacity>
              ))} */}
              </View>
            );
          }
        }

        return (
            <Container style={styles.wrapper}>
            <Header style={styles.header}>
              <Left style={{ flex: 1 }}>
                <Button
                  transparent
                  onPress={() => this.props.navigation.navigate("MainMenu")}
                >
                  <Icon
                    name="ios-arrow-back"
                    size={20}
                    style={styles.facebookButtonIconOrder2}
                  />
                </Button>
              </Left>
              <Body style={{ flex: 3, alignItems: "center" }}>
                <Title style={styles.textbody}>{this.state.stand}</Title>
              </Body>
              <Right style={{ flex: 1 }}>
                {/* <Button transparent onPress={() => this.submit()}>
                  <Icon
                    name="ios-cart"
                    size={20}
                    style={styles.facebookButtonIconOrder2}
                  />
                </Button> */}
              </Right>
            </Header>
            <View style={{ marginTop: 5, marginRight:0}}>
              {list}
            </View>

            <View style={{ width: 270, position: "absolute",}}>
          <Dialog
            visible={this.state.popUp}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogStyle={{ position: "absolute", top: this.state.posDialog }}
            onTouchOutside={() => {
              this.setState({ popUp: false });
            }}
          >
            <DialogContent>
              {
                <View>
                  <View style={{alignItems:'center', justifyContent:'center', paddingTop:20, width:'100%'}}>
                    <Text style={{fontSize:15, alignItems:'center', color:colors.black}}>Keranjang</Text>
                  </View>
                    {/* {this.state.listOrder.map((listOrder, index) => (
                    <View style={{flex:1, flexDirection:'row'}}>
                      <View style={{width:180}}>
                        <Text style={{ paddingTop: 10, fontSize: 15, fontWeight:'bold' }}>
                          {listOrder.nama}
                        </Text>
                      </View>
                      <View style={{width:70}}>
                        <Text style={{ paddingTop:10, fontSize: 15, color:colors.primary  }}>
                          Rp. {listOrder.harga}
                        </Text>
                      </View>
                    </View>
                    ))} */}

                  <View style={{paddingTop:20, width:'100%', alignItems:'center'}}>
                    <View style={{flex:1, flexDirection:'row'}}>
                      <View style={{width:'40%', paddingRight:10}}>
                          <Button
                            block
                            style={{
                              width:'100%',
                              marginTop:10,
                              height: 35,
                              marginBottom: 5,
                              borderWidth: 0,
                              backgroundColor: colors.primary,
                              borderRadius: 15
                            }}
                            onPress={() => this.close()}
                          >
                            <Text style={{color:colors.white}}>Tambah Lagi</Text>
                          </Button>
                      </View>
                      <View style={{width:'40%', paddingRight:10}}>
                          <Button
                            block
                            style={{
                              width:'100%',
                              marginTop:10,
                              height: 35,
                              marginBottom: 5,
                              borderWidth: 0,
                              backgroundColor: colors.primary,
                              borderRadius: 15
                            }}
                            onPress={() => this.navigateToScreen("Order", this.state.listOrder)}
                          >
                            <Text style={{color:colors.white}}>Checkout</Text>
                          </Button>
                      </View>
                    </View>
                  </View>
                </View>
              }
            </DialogContent>
          </Dialog>
        </View>

            </Container>
        );
    }
}
