import React, { Component } from 'react';
import {
  AppRegistry,
  Text,
  View,
  TouchableHighlight,
  NativeAppEventEmitter,
  Platform,
  PermissionsAndroid,
  AsyncStorage,
  Alert,
  ScrollView,
  FlatList,
  TouchableOpacity,
  RefreshControl,
  ActivityIndicator
} from 'react-native';

import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Body,
    Icon,
    Thumbnail
 } from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import styles from "./styles/AllStand";
import BleManager from 'react-native-ble-manager';
import GlobalConfig from "../components/GlobalConfig";
import SubStand from "../components/SubStand";
import colors from "../../styles/colors";

var that;
class ListItem extends React.PureComponent {
  navigateToScreen(route, dataStand) {
    AsyncStorage.setItem("dataStand", JSON.stringify(dataStand)).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View style={{width:'100%', marginRight:0, borderBottomWidth:1, borderBottomColor:'#DEDFDF', marginLeft:15}}>
      <TouchableOpacity
        transparent
        onPress={() => this.navigateToScreen("DetailStand", this.props.data)}>
        <SubStand
            imageUri={{ uri: GlobalConfig.IMAGEHOST + this.props.data.image }}
            namaToko={this.props.data.nama_toko}
            caption={this.props.data.caption}
          />
        </TouchableOpacity>
      </View>
    );
  }
  }

export default class TopUp extends Component {

    constructor(){
        super()
        this.state = {
            ble:null,
            scanning:false,
            token:'',
            listStand:[],
            info:false,
        }
    }

    static navigationOptions = {
        header: null
    };

    onRefresh() {
        console.log("refreshing");
        this.setState({ isLoading: true }, function() {
            this.loadStand();
        });
    }

    componentDidMount() {
        AsyncStorage.getItem("token").then(token => {
            this.setState({
              token: token,
              isLoading: false,
            });
            this.loadStand();
        });
        
        BleManager.start({showAlert: false});
        this.handleDiscoverPeripheral = this.handleDiscoverPeripheral.bind(this);

        NativeAppEventEmitter
            .addListener('BleManagerDiscoverPeripheral', this.handleDiscoverPeripheral );

        if (Platform.OS === 'android' && Platform.Version >= 23) {
            PermissionsAndroid.checkPermission(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
                if (result) {
                  console.log("Permission is OK");
                } else {
                  PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
                    if (result) {
                      console.log("User accept");
                    } else {
                      console.log("User refuse");
                    }
                  });
                }
          });
        }
    }

    loadStand(){
        this.setState({
            isLoading: true,
        })
        var token_Authorization = 'Bearer ' + JSON.parse(this.state.token);
 
        var url = GlobalConfig.SERVERHOST + 'show';
        var formData = new FormData();
        formData.append("mac_address", 'AC:23:3F:26:08:F6,AC:23:3F:26:08:F1')

        fetch(url, {
            headers: {
            'Content-Type': 'multipart/form-data',
            'Authorization': token_Authorization,
            },
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    listStand:responseJson,
                    isLoading: false,
                })
            })
            .catch((error) => {
            Alert.alert('Tidak Ada Internet', 'Cek Koneksi Internet Anda', [{
                text: 'Ok'
            }])
            console.log(error)
            })
    }

    loadInfo(){
      this.setState({
        info:true
      })
    }

    handleScan() {
        BleManager.scan([], 30, true)
            .then((results) => {console.log('Scanning...'); });
    }

    toggleScanning(bool){
        if (bool) {
            this.setState({scanning:true})
            this.scanning = setInterval( ()=> this.handleScan(), 3000);
        } else{
            this.setState({scanning:false, ble: null})
            clearInterval(this.scanning);
        }
    }

    handleDiscoverPeripheral(data){
        console.log('Got ble data', data);
        this.setState({ ble: data })
    }

    _renderItem = ({ item }) => <ListItem data={item} />;

    render() {

        // const container = {
        //     flex: 1,
        //     justifyContent: 'center',
        //     alignItems: 'center',
        //     backgroundColor: '#F5FCFF',
        // }

        const bleList = this.state.ble
            ? <Text> Device found: {this.state.ble.name} </Text>
            : <Text>no devices nearby</Text>

        that = this;
        var list;
        if (this.state.isLoading) {
          list = (
            <View
              style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
            >
              <ActivityIndicator size="large" color="#330066" animating />
            </View>
          );
        } else {
          if (this.state.listStand == '') {
            list = (
              <View
                style={{ flex: 1, justifyContent: "center", alignItems: "center", marginTop:200 }}
              >
                <Thumbnail
                  square
                  large
                  source={require("../../assets/images/empty.png")}
                />
                <Text>Tidak Ditemukan !</Text>
              </View>
            );
          } else {
            list = (
              <FlatList
                data={this.state.listStand}
                renderItem={this._renderItem}
                keyExtractor={(item, index) => index.toString()}
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.isLoading}
                    onRefresh={this.onRefresh.bind(this)}
                  />
                }
              />
            );
          }
        }

        return (
            <Container style={styles.wrapper}>
            <Header style={styles.header}>
              <Left style={{ flex: 1 }}>
                <Button transparent onPress={() => this.loadInfo()}>
                  <Icon
                    name="ios-contact"
                    size={20}
                    style={styles.facebookButtonIconOrder2}
                  />
                </Button>
              </Left>
              <Body style={{ flex: 3, alignItems: "center" }}>
                <Title style={styles.textbody}>I Food Court</Title>
              </Body>
              <Right style={{ flex: 4 }}>
                <Button transparent onPress={() => this.loadInfo()}>
                  <View style={{backgroundColor:'#781116', padding:10, borderRadius:10}}>
                    <Text style={{fontSize:16, color:colors.white, fontWeight:'bold'}}>Rp. 200.000</Text>
                  </View>                
                </Button>
              </Right>
            </Header>
            <Content>
            <View style={{ marginTop: 5, marginRight:20}}>
              {list}
            </View>
            </Content>
            <Footer>
              <FooterTab style={{ backgroundColor:colors.white}}>
                <View style={{flex:1, flexDirection:'row', paddingRight:10}}>
                  <View style={{width:'45%', justifyContent:'center'}}>
                    <View style={{paddingTop:15, paddingLeft:15, paddingBottom:15}}>
                      <Text style={{fontSize:20}}></Text>

                    </View>
                  </View>
                  <View style={{width:'55%', justifyContent: 'center'}}>
                      <Button
                        block
                        style={{
                          width:'100%',
                          height: 40,
                          margin: 5,
                          borderWidth: 0,
                          backgroundColor: colors.primary,
                          borderRadius: 25
                        }}
                        onPress={() => this.loadStand()
                        }
                      >
                        <Text style={{color:colors.white, fontWeight:'bold', fontSize:15}}>SCAN</Text>
                    </Button>
                  </View>
                </View>
              </FooterTab>
            </Footer>
            {/* <TouchableHighlight style={{padding:20, backgroundColor:'#ccc'}} onPress={() => this.toggleScanning(!this.state.scanning) }>
                     <Text>Scan Bluetooth ({this.state.scanning ? 'on' : 'off'})</Text>
                 </TouchableHighlight> */}

            <View style={{ width: 270, position: "absolute",}}>
            <Dialog
              visible={this.state.info}
              dialogAnimation={
                new SlideAnimation({
                  slideFrom: "bottom"
                })
              }
              dialogStyle={{ position: "absolute", top: this.state.posDialog }}
              onTouchOutside={() => {
                this.setState({ info: false });
              }}
            >
              <DialogContent>
                {
                  <View>
                    <View style={{alignItems:'center', justifyContent:'center', paddingTop:20, width:'100%'}}>
                      <Text style={{fontSize:15, alignItems:'center', color:colors.black}}>Hallo !!</Text>
                      <Text style={{fontSize:20, alignItems:'center', color:colors.black, fontWeight:'bold', color:colors.primary, marginTop:5}}>Muhamat Zaenal Mahmut</Text>
                      <Text style={{fontSize:15, alignItems:'center', color:colors.black, marginTop:15}}>Saldo Anda</Text>
                      <Text style={{fontSize:25, alignItems:'center', color:colors.black, fontWeight:'bold', color:colors.primary, marginTop:5}}>Rp. 200.000</Text>
                    </View>
                      
                    <View style={{paddingTop:20, width:'100%', alignItems:'center'}}>

                      <View style={{flex:1, flexDirection:'row'}}>
                        <View style={{width:'80%'}}>
                            <Button
                              block
                              style={{
                                width:'100%',
                                marginTop:10,
                                height: 35,
                                marginBottom: 5,
                                borderWidth: 0,
                                backgroundColor: colors.primary,
                                borderRadius: 15
                              }}
                              onPress={() => this.navigateToScreen("Bank")}
                            >
                              <Text style={{color:colors.white}}>TOP UP</Text>
                            </Button>
                        </View>
                      </View>
                    </View>
                  </View>
                }
              </DialogContent>
            </Dialog>
            </View>
            </Container>




            // <View style={container}>

            //     {/* <Button onPress={() => this.toggleScanning(!this.state.scanning)} title="Start scanning"/> */}

            //     <TouchableHighlight style={{padding:20, backgroundColor:'#ccc'}} onPress={() => this.toggleScanning(!this.state.scanning) }>
            //         <Text>Scan Bluetooth ({this.state.scanning ? 'on' : 'off'})</Text>
            //     </TouchableHighlight>

            //     {bleList}
            // </View>
        );
    }
}
